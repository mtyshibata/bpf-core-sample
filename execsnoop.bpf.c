/* SPDX-License-Identifier: GPL-2.0 OR BSD-3-Clause */
/* SPDX-FileCopyrightText: 2021 Mitsuya Shibata */
#include "vmlinux.h"
#include <bpf/bpf_core_read.h>
#include <bpf/bpf_helpers.h>
#include "execsnoop.h"

struct {
    __uint(type, BPF_MAP_TYPE_RINGBUF);
    __uint(max_entries, 256 * 1024 /* 256 KiB */);
} events SEC(".maps");

struct {
    __uint(type, BPF_MAP_TYPE_HASH);
    __uint(max_entries, 8192);
    __type(key, pid_t);
    __type(value, u64);
} exec_start SEC(".maps");

SEC("tracepoint/syscalls/sys_enter_execve")
int syscalls__execve(struct trace_event_raw_sys_enter *ctx)
{
    struct event *event = NULL;
    struct task_struct *task;
    pid_t pid;
    u64 ts;

    event = bpf_ringbuf_reserve(&events, sizeof(*event), 0);
    if (!event)
        return 0;

    pid = event->pid = bpf_get_current_pid_tgid() >> 32;
    task = (struct task_struct *)bpf_get_current_task();
    event->ppid = (pid_t)BPF_CORE_READ(task, real_parent, tgid);

    if (bpf_get_current_comm(&event->comm, sizeof(event->comm)) < 0)
        goto err;

    if (bpf_core_read_user_str(event->fname, sizeof(event->fname), (const char*)ctx->args[0]) < 0)
        goto err;

    event->duration = 0;
    ts = bpf_ktime_get_ns();
    bpf_map_update_elem(&exec_start, &pid, &ts, BPF_ANY);

    bpf_ringbuf_submit(event, 0);

    return 0;

err:
    bpf_ringbuf_discard(event, 0);
    return 0;
}

SEC("tracepoint/sched/sched_process_exit")
int sched_process_exit(struct trace_event_raw_sched_process_template* ctx)
{
    pid_t pid;
    u64 *start;
    u64 end = 0, duration = 0;
    struct event *event = NULL;
    struct task_struct *task;

    end = bpf_ktime_get_ns();
    pid = bpf_get_current_pid_tgid() >> 32;

    start = bpf_map_lookup_elem(&exec_start, &pid);
    if (!start)
        return 0;
    duration = end - *start;
    bpf_map_delete_elem(&exec_start, &pid);

    event = bpf_ringbuf_reserve(&events, sizeof(*event), 0);
    if (!event)
        return 0;

    task = (struct task_struct *)bpf_get_current_task();
    event->ppid = (pid_t)BPF_CORE_READ(task, real_parent, tgid);
    if (bpf_get_current_comm(&event->comm, sizeof(event->comm)) < 0)
        goto err;

    event->fname[0] = '\0';
    event->duration = duration;

    bpf_ringbuf_submit(event, 0);

    return 0;

err:
    bpf_ringbuf_discard(event, 0);
    return 0;
}

char LICENSE[] SEC("license") = "Dual BSD/GPL";
