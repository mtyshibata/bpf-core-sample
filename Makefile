#!/usr/bin/make -f

APPS = execsnoop

CFLAGS += -g -O2 -Wall
LDFLAGS += $(shell pkg-config --libs libbpf)

ARCH ?= $(shell uname -m)
ARCH := $(patsubst x86_64,x86,$(ARCH))
ARCH := $(patsubst aarch64,arm64,$(ARCH))
ARCH := $(patsubst riscv64,riscv,$(ARCH))

.PHONY: all
all: $(APPS)

$(APPS): %: %.o %.skel.h
	$(CC) $(CFLAGS) $< $(LDFLAGS) -o $@

%.o: %.c %.skel.h execsnoop.h
	$(CC) $(CFLAGS) -c $< -o $@

%.skel.h: %.bpf.o
	bpftool gen skeleton $< > $@

%.bpf.o: %.bpf.c vmlinux.h execsnoop.h
	clang $(CFLAGS) -target bpf -D__TARGET_ARCH_$(ARCH) -c $< -o $@
	llvm-strip -g $@

vmlinux.h:
	bpftool btf dump file /sys/kernel/btf/vmlinux format c > $@

.PHONY: clean
clean:
	-rm -f *.o *.skel.h vmlinux.h $(APPS)

.SECONDARY:
