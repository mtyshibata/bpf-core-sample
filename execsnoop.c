/* SPDX-License-Identifier: CC0-1.0 */

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "execsnoop.skel.h"
#include "execsnoop.h"

int handle_event_cb(void *ctx, void *data, size_t size)
{
    struct event *event = data;
    fprintf(stdout, "%-5s  % 8d  % 8d  % 12lld  %-16s  %-32s\n",
            event->duration ? "END" : "START",
            event->pid, event->ppid, event->duration,
            event->comm, event->fname);

    return 0;
}

int main(void)
{
    struct execsnoop_bpf *obj;
    struct ring_buffer *rb = NULL;
    int err;

    obj = execsnoop_bpf__open();
    if (!obj) {
        fprintf(stderr, "failed to open BPF object\n");
        return 1;
    }

    if (execsnoop_bpf__load(obj)) {
        fprintf(stderr, "failed to load BPF object\n");
        goto cleanup;
    }

    if (execsnoop_bpf__attach(obj)) {
        fprintf(stderr, "failed to attach BPF object\n");
        goto cleanup;
    }

    rb = ring_buffer__new(bpf_map__fd(obj->maps.events), handle_event_cb, NULL, NULL);
    if (!rb) {
        rb = NULL;
        fprintf(stderr, "failed to open ring buffer: %d\n", err);
        goto cleanup;
    }

    fprintf(stdout, "%-5s  %-8s  %-8s  %-12s  %-16s  %-32s\n",
            "STATE", "PID", "PPID", "DURATION", "COMM", "FNAME");
    for (;;) {
        err = ring_buffer__poll(rb, 100 /* ms of timeout */);
        if (err < 0 && errno != EINTR) {
            fprintf(stderr, "failed perf_buffer_poll(): %s\n", strerror(errno));
            goto cleanup;
        }
    }

cleanup:
    ring_buffer__free(rb);
    execsnoop_bpf__destroy(obj);
    return 0;
}
