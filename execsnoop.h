/* SPDX-License-Identifier: CC0-1.0 */
#ifndef __EXECSNOOP_H__
#define __EXECSNOOP_H__

#define TASK_COMM_LEN 16 /* from linux/sched.h */

struct event {
    pid_t pid;
    pid_t ppid;
    char comm[TASK_COMM_LEN];
    char fname[32];
    __u64 duration;
};
#endif /* __EXECSNOOP_H__ */
